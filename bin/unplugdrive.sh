#!/bin/bash
# unplugdrive.sh
#
# Allows safe unplugging of removable storage devices.
# Handles simultaneous selection of multiple drives
# Checks whether devices can safely get unmounted
# Unmounts all mounted partitions on nominated drive(s)
# Checks whether mountpoints failed unmounting
# spins down true rotational devices
#
# If user is expected to be able to start unplugdrive without being asked for entering his sudo password, it is necessary to add unplugdrive to the /etc/sudoers.d file with the "NOPASSWORD" option from ver. 0.82c and above.
# System settings referring to unplugdrive need to get supplemented by sudo preceding unplugdrive from ver. 0.82c and above. If etc/sudoers.d/antixers is modified at the same time, user won't notice any difference of operation compared to precvious version 0.82.
# Additionally an alias like "alias unplugdrive='sudo unplugdrive.sh'" and/or "alias unplugdrive.sh='sudo unplugdrive.sh'" should be added to system settings for user convenience, from ver 0.82c and above.
#
# Requires true bash (tested on GNU bash, Version 5.0.3(1)-release (i686-pc-linux-gnu))
# Dependencies: GNU bash, yad, pmount, sudo, whoami, findmnt, mapfile, grep, cut, lsblk, sort, sync, tr, sed, rev, cat, pidof, kill, hdparm
# Requires /etc/udev/rules.d/99-usbstorage.unused to be renamed 99-usbstorage

# bobc 12/28/19 added safemode, default FALSE, to avoid confirmation dialogs
# ppc 20/11/2020 - changed some design, making the dialog windows seem more "inbuit" with the OS, added time out to warning messages, and a title to the windows... cleaned up a bit the selection dialog so it's more multi-lingual...
# Robin.antiX 04/14/2021 new feature: debug mode structure added for easier testing.
# Robin.antiX 04/14/2021 bugfix: script did not unmount drives mounted to multiple mountpoints correctly, reporting to user it was safe to unplug while device was still mounted. First fixings by streamlining the processing.
# Robin.antiX 04/14/2021 bugfix: script did not unmount drives containing underline character "_" in mountpoint (happened always when parition-lable contained this character); fixed by removing seperator replacement and added different IFS instead.
# Robin.antiX 04/18/2021 bugfix: script did failed unmounting any usb drives not visable to df command, fixed by adding -a option.
# Robin.antiX 04/18/2021 bugfix: when more than 2 devices were to be unmounted lower checkboxes not accessible when using classical 3-button mouse. Slider at right side without function, so fixed by deriving window hight from number of drives/mountpoints displayed.
# Robin.antiX 04/18/2021 GUI behaviour: added -center to yad dialogs to prevent script windows to pop up in random, unforseeable places somewhere on screen. Moreover white borderless windows randomly scattered on white background from other open documents, resulting in text messages hiding within other text.
# Robin.antiX 04/18/2021 new features: commandline switches for help, safemode, decorations and debugging. Added taskbar icon in order to prevent yad from using its erlenmeyer flask.
# Robin.antiX 04/18/2021 bugfix: wrong entries in drivelist replaced (should have been TRUE/FALSE instead of position number for initialising checkboxes correctly)
# Robin.antiX 04/20/2021 bugfix: check for remaining devices, did still not reliably detect leftover mountpoints.
# Robin.antiX 04/20/2021 bugfix: deriving amount of time needed for sync from system info instead of fixed value; moreover before the delay window was not closing after sync due to “$!” mismatch of yad pids.
# Robin.antiX 05/04/2021 new feature: "centred" optional for users preferring opening windows near mouse pointer. Additional command line option: -c or --centred.
# Robin.antiX 05/06/2021 bugfix: replaced „pmount” by „umount” command for actual unmounting devices for several issues (pmount mixing up mountpoints for identical partitions and unfounded "Error: could not determine real path of the device" messages.)
# Robin.antiX 06/08/2021 bugfix: mechanical (rotational) USB hdds did not spun down after unmounting, so it was not safe to unplug them wheras script tells user it was. Therefore it was neccessary to change default behaviour from “unsafe” to “safe” so user has to deactivate support for spinning devices actively by passing -u or -i option.
# Robin.antiX 06/13/2021 bugfix: complete code rewrite for ver. 0.84 , geting rid of confusing variety of unconnected lists, replacing them by correlated arrays. New engine design allows to handle mountpoints containing blanks, and check for nested mounts (e.g. used on antiX live systems). New clear dialog design, so no extra confirmation dialogs needed anymore.
# Robin.antiX 10/21/2021 new feature in ver 0.90: supports closing of luks encrypted partitions.
# Robin.antiX 10/21/2021 unplugdrive ver. 0.91 is able to run with sudo and without as well. Some of its features may still not work without using sudo.

### Please Note: text strings for debug output dont't need to get marked for translation.


# 0.) Preliminaries
#-------------------

# Variable declaration and stting of default values
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=unplugdrive.sh

title=$"Unplug USB device"
version="0.91"
txt_version=$"Version "$version					#text string for translation

icon_taskbar="/usr/share/icons/numix-square-antix/48x48/devices/usbpendrive_unmount.png"

#tempfile_01=/tmp/unplugdrive-$$.sh    # no longer needed
#debuglog="$HOME/unplugdrive-debug-$(date +%d%m%Y%H%M%S).log"	# function to write a logfile not implemented yet

count_items=0									# stores number of items found in correlated arrays (A)...(I).
mnt_devices_names=()							#(A)	# e.g. sdc, sdb, mmcblck3
mnt_devices_paths=()							#(B)	# e.g. /dev/sdc, /dev/sdb, /dev/mmcblck3
mnt_partitions_names=()							#(C)	# e.g. sdc4, sdb2, mmcblck3p1
mnt_partitions_paths=()							#(D)	# e.g. /dev/sdc4 /dev/mmcblck3p1
mountpoints_paths=()							#(E)	# e.g. /media/demo/sdd4-usb-256MB_USB2.0Flas, /media/demo/mmcblk3p1-mmc-SD02G_0x35962817, 
mnt_devices_rotational=()						#(H)	# true/false
device_is_unmounted=()							#(I)	# true/false
item_blocked=()									#(F)	# true/false
blocked_reason=()								#(G)	# takes reasons for excluding partitions of a device from unmounting. Textstring: Items separated by "tab" and Partitions searated by "|", postition referring to count of correlated arrays.
blocked_reason_part=""							# takes reason for excluding a partition from unmounting
#count_blocked_items=0
main_user_dialog=""								# takes the complete yad command created on runtime for user dialog.
spindownlist=""									# informational list for display purpose
spinningdownlist=""								# informational list for display purpose
summarylist=""									# informational list for display purpose
txt_rotationalerror=""							# informational list for display purpose
txt_part=""
len_txt_part=0
flag_umounterror=0
flag_spindown=0
flag_debugmode=0										# functional flag by changed by command line argument
flag_dryrun=0											# functional flag by changed by command line argument
flag_ignore_rotational=0								# functional flag by changed by command line argument
flag_nosudo=false										# functional flag by changed by detection of sudo status
decorations="--undecorated"								# referring to default dialog display frame undecorated/with control elements, changed by command line argument
centred=""												# referring to default dialog display position centred/near mouse, changed by command line argument
flag_unsafe="false"										# referring to default mode/unsafe mode, changed by command line argument
CHR_0x0A="$(printf '\nx')"; CHR_0x0A=${CHR_0x0A%x}		# create special seperator
txt_on=$"on" 											# refers to message “<partition> on <mountpoint>” presented in dialog to make it translatable.
txt_dlg_blocked=$"Blocked by nested mount"				# text string for translation; refers to message in user dialog informing about greyed out (deactivated) device for unmount.
txt_dlg_header=$"Mounted USB Partitions"				# text string for translation;
txt_dlg_instruction=$"Choose the drive(s) to be unplugged:"		# text string for translation;
txt_dlg_encrypted=$"Encrypted devices — will get closed if selected for unplugging."
txt_dlg_device=$"Device"								# text string for translation;
txt_dlg_button_1=$"Abort"								# text string for translation;
txt_dlg_button_4=$"Proceed"								# text string for translation;
true_user=$SUDO_USER							# variable takes actual role of user starting the script (root/normal user name)
orig_IFS="$IFS"									# store original field seperator

# check for commandline options
txt_cline="$*"
while [[ $# -gt 0 ]]
do
  opt_cline="$1"
  case $opt_cline in
    -h|--help)	      echo ""
                      echo $"  Unplugdrive" "($txt_version)"
                      echo ""
                      echo -e $"  GUI tool for safely unplugging removable USB devices."
                      echo ""
                      echo $"  Usage: unplugdrive.sh [options]"
                      echo ""
                      echo $"  Options:"
                      echo -e $"\t-h or --help\t\tdisplays this help text"
                      echo -e $"\t-u or --unsafe\t\truns script in unsafe mode,\n\t            \t\tomiting some checks and dialogs"
                      echo -e $"\t-i or --ignore\t\tignores whether devices are\n\t            \t\treported as rotational even in\n\t            \t\tdefault mode. No spindown."
                      echo -e $"\t-d or --decorated\tuses window decorations"
                      echo -e $"\t-c or --centred\t\topen dialogs centred"
                      echo -e $"\t-g or --debug\t\tenable debug output (terminal)"
                      echo -e $"\t-p or --pretend\t\tdry run, don't actually un-\n\t            \t\tmount drives (for debugging)"
                      echo -e ""
                      echo -e $"  NEVER use the options -u and -i on rotational devices!"
                      echo ""
                      echo -e $"  Questions, Suggestions and Bugreporting please to:"
                      echo -e "\t<forum.antiXlinux.com>"
                      echo -e ""
                      echo -e "  Copyright 2011, 2012-2018 SamK, anticapitalista"		# entry needed to comply with EU legislation
                      echo -e "  Copyright 2019, 2020, 2021 the antiX comunity"			# entry needed to comply with EU legislation
                      echo -e ""										# This GPL text may not get translated. See gnu.org for details.
                      echo -e "  This program is free software: you can redistribute it and/or"
                      echo -e "  modify it under the terms of the GNU General Public License as"
                      echo -e "  published by the Free Software Foundation, either version 3 of"
                      echo -e "  the License, or (at your option) any later version."
                      echo ""
                      echo -e "  This program is distributed in the hope that it will be useful,"
                      echo -e "  but WITHOUT ANY WARRANTY; without even the implied warranty of"
                      echo -e "  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
                      echo -e "  GNU General Public License for more details."
                      echo ""
                      echo -e "  You should have received a copy of the GNU General Public"
                      echo -e "  License along with this program.  If not, see "
                      echo -e "  \t<http://www.gnu.org/licenses/>."
                      echo ""
                      exit 0;;
    -d|--decorated)   decorations=""
                      shift;;
    -c|--centred)     centred="--center"
                      shift;;                      
    -u|--unsafe)      flag_unsafe="FLASE"
                      shift;;
    -i|--ignore)      flag_ignore_rotational=1
                      shift;;
    -g|--debug)       flag_debugmode=1
                      shift;;
    -p|--pretend)     flag_dryrun=1
                      shift;;                      
	 *)               echo -e $"Invalid command line argument. Please call\nunplugdrive -h or --help for usage instructions."
					  exit 1
                      shift;;
  esac
done

[ $flag_debugmode == 1 ] && echo -e "Version:\t$version\nDebug mode: on""\n$txt_cline\nsafemode:\t$flag_unsafe\ndecorations:\t$decorations\ncentred:\t$centred\ndryrun (pretend):\t$flag_dryrun\nignore rotationals\t$flag_ignore_rotational\nTrue user:\t$true_user\nexecuted by:\t`whoami`"

# check whether unplugdrive is run with sudo by normal user.
	if [ `whoami` != "root" ]; then
		if [ "$true_user" != "root" ]; then echo "“unplugdrive” should be run with SUDO." && if true_user=""; then true_user=$(whoami); flag_nosudo=true; fi; else echo "“unplugdrive” should not be run by root, please use it being normal user." && exit 1; fi
	else
		if [ "$true_user" == "root" ] || [ "$true_user" == "" ]; then echo "“unplugdrive” should not be run by root, please use it being normal user." && exit 1; fi
	fi

# check for needed helper programs etc.:
[ ! $(command -v yad) ] && echo $"\nˮyadˮ is not installed.\n   --> Please install ˮyadˮ before executing this script.\n" && exit 1
[ -e icon_taskbar ] && echo $"Taskbar icon $icon_taskbar not found."


# preparing cleanup on leaving
function cleanup {
[ -e $tempfile_01 ] && rm -f $tempfile_01
IFS="$orig_IFS"
# echo "mischief managed!"												# Uncomment this line to check whether cleanup is executed under any exit conditions.
return 0
}
trap 'cleanup && exit 1' SIGINT
#>>$tempfile_01

# prepare safemode message
function safemode {
if [ "$flag_unsafe" = "false" ]; then
  sudo -u $true_user -- yad --title="$title" --fixed $centred --timeout=3 --mouse $decorations \
      --window-icon="$icon_taskbar" \
      --text=$"<b>Aborting</b> on user request\n<b>without unmounting.</b>" \
      --no-buttons
  buttonselect=$?
  [ $buttonselect == 252 ] || [ $buttonselect == 255 ] && buttonselect=1
  [ $buttonselect == 70 ] && buttonselect=1
fi
return 0
}

# prepare error message for unclosed encrypted partitions
function close_encrypt_error {
sudo -u $true_user -- yad --title="$title" --fixed $centred --mouse $decorations --borders=10 \
      --window-icon="$icon_taskbar" \
      --text "<b><big>"$"Closing Encryption failed.""</big></b>\n""<span foreground='red'><u><b>"$"The following encrypted partition(s) could not get closed:""</b></u></span>\n\t$1\n<b>"$"Please make sure these are closed before proceeding!""</b>.\n "$"If you can't close an encrypted device, please abort unmounting.""\n" \
      --button=$"Abort unmounting":4 --button=$"I have closed them now. Proceed":6
buttonselect=$?
[ $buttonselect == 252 ] || [ $buttonselect == 255 ] && buttonselect=1
[ $buttonselect == 70 ] && buttonselect=4
if [ $buttonselect == 4 ]; then
	cleanup
	exit 1	
fi
return 0
}


# 1.) Gather information about unpluggable devices and analysis for both, unmounting and user dialog:
#----------------------------------------------------------------------------------------------------
# Turned out pmount is best way to determine what pluggable devices with mounted partitions are present on system.
# Alternative: cat /sys/block/$device/removable, unfortunately this doesn't correctly reflects the removable state for sd-cards and usb-hdds for some reason (bug?).
# For mmc devices (sd-cards) we need to distinguish between removable and non removable types: /sys/block/$dev/device/type, but pmount does the job. So its probably best choice.
# But pmount output isn't too much scriptable (using blanks for separation of items, causing we can't distinguish from blanks within itmes), so we'll use it to grep and cut the first column only, for deriving unique devices names of mounted partitions.

# additional remark for maintnance: since bash doesn't support multidimensional arrays by now we need strictly to observe the correlation of arrays compound by item numbers while processing.
# There are two kinds of correlation: inner and outer interrelation. Firstly the item number, identical in all correlated arrays (outer correlation), referring to a specific pluggable device stored.
# Secondly, within each item of outer correlation, the position number within strings refers to the different partitions of a device (inner correlation). Dependend on needs these are either seperated by nul-termination or "|", but always stricly correlated across identical items of outer correlation of compound arrays.
# The correlated (compound) arrays are marked by #(A)...#(I). Recent number of items in compound arrays is stored in $count_items.


# 1.1.) create list (nul-terminated string) of devicenames of mounted partitions on pluggable devices, omit CD/DVD-ROM (sr*)
pm_partitions_names="$(pmount | cut -sd"/" -f 3 | sed 's/ on $//' | sort -u)"				# use first column from output of pmount only to avoid cut blanks in mountpaths.

# debug-check
if [ $flag_debugmode == 1 ]; then echo -e "\n1.1. Devices of mounted Partitions\n----------------------------------\nfound partitions names (\$pm_partitions_names):\n$pm_partitions_names\n"; fi

# lookup devicesnames for found mounted partitions on unpluggable devices and put them in an array
if [ -n "$pm_partitions_names" ]; then
	v=0;
	while read -d"$CHR_0x0A" item; do
		[ $flag_debugmode == 1 ] && echo -e "found item in \$pm_partitions_names:\t$item"
		v=${#mnt_devices_names[@]}
		mapfile -t -O $v mnt_devices_names <<<"$(basename "$(readlink -f "/sys/class/block/$item/..")")"			#(A)
		[ $flag_debugmode == 1 ] && echo -e "-- entered item into \$mnt_devices_names[@]:\t${mnt_devices_names[$v]}"
	done <<<"$pm_partitions_names"


# 1.2.) remove duplicates from $mnt_devices_names[@]
	mapfile -t mnt_devices_names <<<"$(printf '%s\n' "${mnt_devices_names[@]}" | sort -u)"

	#debug-check
	if [ $flag_debugmode == 1 ]; then
		echo -e "\n1.2./1.3. Unique devices\n------------------------"
		v=0
		echo -e "\ndevices names (duplicates removed) in \${mnt_devices_names[@]}:"							#(A)
		for item in "${mnt_devices_names[@]}"; do															#(A)
			echo -e "$((v++))\t$item"
		done
	fi

# 1.3.) store total number of unique devices, which is the expected number of items in each of the correlated arrays
	count_items="${#mnt_devices_names[@]}"
	[ $flag_debugmode == 1 ] && echo -e "\nTotal number of items (\$count_items):\t$count_items"
else
	[ $flag_debugmode == 1 ] && echo -e "\n1.2./1.3. No unplugable drives found\n------------------------\nexiting."
	count_items=0
fi

# 1.4.) Display a message if no candidate for unmounting was discovered and exit
if [ "$count_items" == 0 ];then
   sudo -u $true_user -- yad --title="$title" --fixed $centred --timeout=5 --mouse $decorations \
       --window-icon="$icon_taskbar" \
       --text $"A removable drive with a mounted\npartition was not found.\n<b>It is safe to unplug the drive(s)</b>" \
       --no-buttons 
   cleanup
   exit 0
fi

# 1.5.) store basic properties of found devices to compund arrays, alltogether correlated to count found in in array ${mnt_devices_names[n]}:
# look up devices paths and store these in the array ${mnt_devices_paths[n]} #(B)
# look up partitions paths and store them (as nul-terminated string) into the array ${mnt_partitions_paths[n]} #(D)
# derive partitions names and store them (as nul-terminated string) into the array ${mnt_partitions_names[n]} #(C)
# look up mountpoints of each partition and put them (as separate tab-terminated string, partitions seperated by "|") into the array ${mountpoints_paths[n]} #(E)
# look up whether device is reported as rotational and store information in array ${mnt_dev_is_rotational[n]} #(H)
v=0
while [ $v -lt $count_items ]; do
	mnt_devices_paths+=("$(echo "/dev/${mnt_devices_names[$v]}")")																#(B)
	mnt_partitions_paths+=("$(lsblk -no PATH,TYPE ${mnt_devices_paths[$v]} | grep part | cut -d" " -f 1)")						#(D)
	mnt_partitions_names+=("$(lsblk -no PATH,TYPE ${mnt_devices_paths[$v]} | grep part | cut -d" " -f 1 | cut -d"/" -f 3)")		#(C)
	while read -d"$CHR_0x0A" item; do
		mountpoints_paths[$v]="${mountpoints_paths[$v]}$(findmnt -lunUo target $item | tr '\n' '\t')|"							#(E)	let's use "|" for seperator of devices since it is most unlikely to met it in any name given by manufacturer
	done <<<"${mnt_partitions_paths[$v]}"
	mountpoints_paths[$v]="$(echo "${mountpoints_paths[$v]}" | sed 's/\t|/|/g')"
	mnt_devices_rotational+=("$(if [ $(cat /sys/block/$(echo ${mnt_devices_names[$v]} | cut -d '/' -f 3)/queue/rotational) == 1 ]; then echo "true"; else echo "false";fi)")  #(H)
	let $((v++))
done

# debug-check
if [ $flag_debugmode == 1 ]; then
	v=-1
	echo -e "\n1.5. Total arrays:\n------------------"
	while [ $((++v)) -lt $count_items ]; do
		echo -e "\n-- for device $v:"
		echo -e "(A) \${mnt_devices_names[$v]}:\t${mnt_devices_names[$v]}"				#(A)
		echo -e "(B) \${mnt_devices_paths[$v]}:\t${mnt_devices_paths[$v]}"				#(B)
		echo -e "(C) \${mnt_partitions_names[$v]}:\t${mnt_partitions_names[$v]}"		#(C)
		echo -e "(D) \${mnt_partitions_paths[$v]}:\t${mnt_partitions_paths[$v]}"		#(D)
		echo -e "(E) \${mountpoints_paths[$v]}:\t${mountpoints_paths[$v]}"				#(E)
		echo -e "(H) \${mnt_devices_rotational[$v]}:\t${mnt_devices_rotational[$v]}"	#(H)
	done
fi

# 1.6. check for mounted filesystem images on devices

# debug-check
[ $flag_debugmode == 1 ] && echo -e "\n1.6. Check for mounted filesystem images stored on devices:\n-----------------------------------------------------------" 

# run through each device stored in arrays
v=0
while [ $v -lt $count_items ]; do
	[ $flag_debugmode == 1 ]&& echo -e "\n1.6.1.$(($v+1)) -- device $v (${mnt_devices_names[$v]}):\n--------------------------"
	device_is_unmounted[$v]="true"				#(I)
	item_blocked[$v]="false"
	blocked_reason_part=""
	count_part="$(echo "${mnt_partitions_names[$v]}" | tr '|' '\n' | wc -l)"
	# run through each partition of recent device
	w=0
	while [ $((++w)) -le $count_part ]; do
		# seperate correlated mountpoints from string
		mountpoints_paths_part="$(echo "${mountpoints_paths[$v]}" | cut -d"|" -f $w | tr '\t' '\n')"
		[ $flag_debugmode == 1 ] && echo -e "\nTotal mountpoints on device $v partition $w (of $count_part) ($(echo ${mnt_partitions_names[$v]} | cut -f 1 | cut -d " " -f $w )):\n$mountpoints_paths_part\n"
		# check each mountpoint whether it is found as source of any other mount.
		u=0
		while read -d"$CHR_0x0A" item; do
			[ $flag_debugmode == 1 ] && echo -e "Checking Mountpoint $((u++)) (\$item):\t$item"
			if [ -n "$item" ]; then
				device_is_unmounted[$v]="false"				#(I)
				item2="$(mount | grep ^"$item")"
				[ $flag_debugmode == 1 ] && echo -en "It is mounted to \$item2:\n$item2"
			else
				[ $flag_debugmode == 1 ] && echo -en "(not mounted)\n"
			fi
			if [ -n "$item2" ]; then
				item_blocked[$v]="true"												#(F)
				blocked_reason_part="$blocked_reason_part$CHR_0x0A$(echo "$item2" | sed 's/^.*on //' | sed 's/ type.*$//')"
				[ $flag_debugmode == 1 ] && echo -e "\nReason for blocking (\$blocked_reason_part):\t$blocked_reason_part"
			else
			[ $flag_debugmode == 1 ] && echo -en "(empty)\n"
			blocked_reason_part="$blocked_reason_part$CHR_0x0A" # add list item separator
			fi
		done <<<"$mountpoints_paths_part"
		blocked_reason_part="$blocked_reason_part|" # add partition seperator
	done
	if "${item_blocked[$v]}"; then
		blocked_reason[$v]="$(echo "$blocked_reason_part" | tr '\n' '\t' | tr -s '\t' )"		#(G)
		# let $((count_blocked_items++))
		[ $flag_debugmode == 1 ] && echo -e "\n!!! Device $v (${mnt_devices_names[$v]}) is blocked by nested mount(s):\n${blocked_reason[0]}\nFlag \$(item_blocked[@]): ${item_blocked[$v]}"
	else
		[ $flag_debugmode == 1 ] && echo -e "\nDevice $v (${mnt_devices_names[$v]}) is not blocked by nested mounts."
	fi
	let $((v++))
done

# 1.7. Check for encrypted file systems
[ $flag_debugmode == 1 ] && echo -e "\n1.7. Check for encrypted partitions stored on devices:\n-----------------------------------------------------------" 
v=0
while [ $v -lt $count_items ]; do
	[ $flag_debugmode == 1 ] && echo -e "\nChecking partitions of device $v"
	part_encrypted=""
	while read item; do
			if ! $flag_nosudo; then
				part_encrypted="$part_encrypted$(if cryptsetup isLuks $item; then echo -n true; else echo -n false; fi)|"
			else
				part_encrypted="$part_encrypted$(if gksudo -u root -- cryptsetup isLuks $item; then echo -n true; else echo -n false; fi)|"
			fi
	done <<<"${mnt_partitions_paths[$v]}"
	[ $flag_debugmode == 1 ] && echo -en "$part_encrypted\n"
	mnt_devices_encrypted[$v]="$(echo "$part_encrypted" | tr '\n' '\t' | tr -s '\t' )"		#(K)
	let $((v++))
done

# debug-check
if [ $flag_debugmode == 1 ]; then
	echo -e "\n1.6/1.7. Additional arrays:\n------------------------"
	v=-1
	while [ $((++v)) -lt $count_items ]; do
		echo -e "\n-- for device: $v"
	    echo -e "(F) \$item_blocked[$v]:\t${item_blocked[$v]}"							#(F)
		echo -e "(G) \$blocked_reason[$v]:\t${blocked_reason[$v]}"						#(G)
		echo -e "(I) \${device_is_unmounted[$v]}:\t${device_is_unmounted[$v]}"			#(I)
		echo -e "(K) \${mnt_devices_encrypted[$v]}:\t${mnt_devices_encrypted[$v]}"		#(K)		
	done
fi


# 2.) User dialog for selection
# -----------------------------
[ $flag_debugmode == 1 ] && echo -e "\n2. Dialog window\n---------------"

# build up a string variable containing complete yad window command
main_user_dialog=$(echo -e "sudo -u $true_user -- yad --fixed $centred --mouse \\
--title=\"$title\" --width=250 --height=300 \\
--window-icon=\"$icon_taskbar\" $decorations --borders=5 \\
--form --separator=\"|\" --item-separator=\"|\" \\")
main_user_dialog="$main_user_dialog$(echo -e "\n--field=\"<b><big>$txt_dlg_header</big>\n$txt_dlg_instruction</b>\":LBL \\")"
dlg_args="\"\""		# collect arguments for user dialog fields each time a field was added
v=-1
encrypt=false
while [ $((++v)) -lt $count_items ]; do
	main_user_dialog=$main_user_dialog$(echo -e "\n--field="":LBL \\")
	dlg_args="$dlg_args \"\""	
	if ${item_blocked[$v]}; then							# disable (grey out) blocked devices
		main_user_dialog="$main_user_dialog$(echo -e "\n--field=\"$txt_dlg_device ${mnt_devices_names[$v]}\t($txt_dlg_blocked)\":CHK \\")"
		dlg_args="$dlg_args \"@disabled@\""
	else
	    # main_user_dialog="$main_user_dialog$(echo -e "\n--field=\"<b>${mnt_devices_names[$v]}</b>\":CHK \\")"   yad checkbox obviously doesn't accept typographic qualifiers in label. So we can't print this in bold.
		main_user_dialog="$main_user_dialog$(echo -e "\n--field=\"$txt_dlg_device ${mnt_devices_names[$v]}\":CHK \\")"
		dlg_args="$dlg_args \"false\""
	fi
	count_part="$(echo "${mnt_partitions_names[$v]}" | tr '|' '\n' | wc -l)"
	w=0
	while [ $((++w)) -le $count_part ]; do
		txt_part="<u>$(echo ${mnt_partitions_names[$v]} | cut -f 1 | cut -d " " -f $w )</u>"
		if $(echo ${mnt_devices_encrypted[$v]} | cut -f 1 | cut -d "|" -f $w ); then txt_part="$txt_part*"; encrypt=true ;fi
		len_txt_part=$(echo -n "$txt_part" | wc -m)
		mountpoints_paths_part="$(echo "${mountpoints_paths[$v]}" | cut -d"|" -f $w | tr '\t' '\n')"		
		u=0
		while read -d"$CHR_0x0A" item; do
			main_user_dialog=$main_user_dialog$(echo -e "\n--field=\"\t\t$txt_part\t$item\":LBL \\")
			if ${item_blocked[$v]}; then					# disable (grey out) blocked devices
				dlg_args="$dlg_args \"@disabled@\""
			else
				dlg_args="$dlg_args \"\""
			fi
			if [ $len_txt_part -ge 15 ]; then				# layout spaces to meet columns. character count contains typographyc qualifiers! 
				txt_part="\t\t\t"
			elif [ $len_txt_part -ge 12 ]; then
				txt_part="\t\t"
			else
				txt_part="\t"
			fi
		done <<<"$mountpoints_paths_part"
	done
done
main_user_dialog=$main_user_dialog$(echo -e "\n--field="":LBL \\")
dlg_args="$dlg_args \"\""
if $encrypt; then main_user_dialog=$main_user_dialog$(echo -e "\n--field=\"* $txt_dlg_encrypted\":LBL \\"); dlg_args="$dlg_args \"@disabled@\""; fi
main_user_dialog=$main_user_dialog$(echo -e "\n$dlg_args \\")
main_user_dialog=$main_user_dialog$(echo -e "\n--button=\"$txt_dlg_button_1\":1 \\")
main_user_dialog=$main_user_dialog$(echo -en "\n--button=\"$txt_dlg_button_4\":4")

# display dialog and analyse output
[ $flag_debugmode == 1 ] && echo -e "$main_user_dialog"
while [ -z "$dlg_response" ]  # Don't proceed as long nothing was selected, or abort.
	do
	dlg_response="$(echo "$(eval "$main_user_dialog" | tr -s '|'; echo ${PIPESTATUS[0]})" | tr -d '\n')"			# here user dialog is actually displayed.
	buttonselect="$(echo "$dlg_response" | rev | cut -d "|" -f 1)"
	dlg_response="$(echo "$dlg_response" | rev | cut -d "|" -f 2- | rev )"
	if [ $(echo "$dlg_response" | grep "TRUE") ]; then
		dlg_response="$(echo "$dlg_response" | sed 's/^|//')"
	else
		dlg_response=""
	fi
	[ $buttonselect == 252 ] || [ $buttonselect == 255 ] && buttonselect=1		# Catch ESC button and X-icon also 
	[ $buttonselect == 1 ] && break
done
[ -z "$dlg_response" ] && safemode
[ $buttonselect == 70 ] || [ $buttonselect == 1 ] && cleanup && exit 1										# exit for both, normal and safe mode.
[ $flag_debugmode == 1 ] && echo -e "\ndlg_response: $dlg_response\nbuttonselect: $buttonselect"


# 3.) Preparations for unmounting
#--------------------------------
# since new main user dialog is arranged more clearly there is no chance user mixes up the checkboxes, and live boot device is protected (greyed out) now,
# there is no need for an additional confirmation dialog anymore. It was dropped from ver 0.84 onwards.

# 3.1 close encrtypted partitions
if $encrypt; then
	echo -e $"Please wait until encrypted file systems are closed..."
	# get yad pid of specific yad window ($! will fail on yad windows)
	yad_pid_before="$(pidof -x yad) "			# blank suffix intentionally
	sudo -u $true_user -- yad --title="$title" --fixed $centred --mouse $decorations \
		--text=$"Encrypted partitions\nare closed. <b>Please wait...</b>" \
		--window-icon="$icon_taskbar" \
		--no-buttons &
	sleep 1										# wait for second yad pid
	yad_pid="$(pidof -x yad)"
	if [ "$yad_pid_before" != " " ]; then		# if no items sed will fail
		while read -d' ' item
		do
			yad_pid="$(echo "$yad_pid" | sed "s/$item//g")"
		done <<<"$yad_pid_before"
	fi
	txt_dlg_enc_error=""
	v=0
	while [ $v -lt $count_items ]; do
		if [ "$(echo "$dlg_response" | cut -d '|' -f $(($v+1)))" == "TRUE" ]; then
			[ $flag_debugmode == 1 ] && echo -e "\nClosing encryption of partitions on device $v"
			w=1
			while read item; do
				if $(echo ${mnt_devices_encrypted[$v]} | cut -f 1 | cut -d "|" -f $w ); then
					enc_mnt="$(lsblk -ln "$(echo "${mnt_partitions_paths[$v]}" | cut -f 1 | cut -d "$CHR_0x0A" -f $w )" | grep crypt | rev | cut -d" " -f 1 | rev)"
					if [ "$enc_mnt" != "" ]; then
						enc_mnt_dev="$(sudo -u $true_user -- findmnt --list -n -o SOURCE "$enc_mnt")"
						[ $flag_debugmode == 1 ] && echo "- device $enc_mnt_dev"
						if ! $flag_nosudo; then
							umount "$enc_mnt"
							cryptsetup luksClose "$enc_mnt_dev"; if [ "$?" != 0 ]; then txt_dlg_enc_error="$txt_dlg_enc_error$(echo ${mnt_partitions_names[$v]} | cut -f 1 | cut -d " " -f $w ), "; fi
						else
							gksudo -u root -- umount "$enc_mnt"
							gksudo -u root -- cryptsetup luksClose "$enc_mnt_dev"; if [ "$?" != 0 ]; then txt_dlg_enc_error="$txt_dlg_enc_error$(echo ${mnt_partitions_names[$v]} | cut -f 1 | cut -d " " -f $w ), "; fi
						fi
					fi
				fi
				let $((w++))
			done <<<"${mnt_partitions_paths[$v]}"
		fi
		let $((v++))
	done
	kill $yad_pid
	if [ "$txt_dlg_enc_error" != "" ]; then close_encrypt_error "$(echo $txt_dlg_enc_error | rev | cut -c3- | rev)" ;fi
fi

# 3.2 sync before unplugging

# Sync device and ensure user waits long enough before unplugging so everything gets written to storage
[ $flag_debugmode == 1 ] && echo -e "\n3.) Preparations for unmounting\n-------------------------------"

# display user information first
if [ "$flag_unsafe" = "false" ]; then
	# get yad pid of specific yad window ($! will fail on yad windows)
	yad_pid_before="$(pidof -x yad) "				# blank suffix intentionally
	sudo -u $true_user -- yad --title="$title" --fixed $centred --mouse $decorations \
        --text=$"Data is being written\nto devices. <b>Please wait...</b>" \
        --window-icon="$icon_taskbar" \
        --no-buttons &
	buttonselect=$?
	[ $buttonselect == 252 ] || [ $buttonselect == 255 ] && buttonselect=1		# Catch ESC button and X-icon also 
	[ $buttonselect == 70 ] || [ $buttonselect == 1 ] && cleanup && safemode && exit 1
	sleep 1											# wait for second yad pid
	yad_pid="$(sudo -u $true_user -- pidof -x yad)"
	if [ "$yad_pid_before" != " " ]; then			# if no items sed will fail
		while read -d' ' item
		do
			yad_pid="$(echo "$yad_pid" | sed "s/$item//g")"
		done <<<"$yad_pid_before"
	fi
fi

# Write data to device(es)
[ $flag_debugmode == 1 ] && echo -e "Syncing write cache to device"
sudo -u $true_user -- sync

# Wait and close delay window only after sync is done
if [ "$flag_unsafe" = "false" ]; then
	i=0
	while :;
	do
		if [ "$(cat /proc/meminfo | grep 'Dirty:' | tr -s ' ' | cut -d' ' -f 2)" == "0" ] && [ "$(cat /proc/meminfo | grep 'Writeback:' | tr -s ' ' | cut -d' ' -f 2)" == "0" ]; then
			if [ $flag_debugmode == 1 ]; then echo "sync done"; fi; break
		fi
		sleep .2				# wait until sync is done before next check.
		i=$[$i+1]
		if [ $i -ge 5 ]; then
			i=0
			sudo -u $true_user -- sync		#feed some additional sync commands in case first check was missed on systems under heavy load.
		fi
	done
	[ $flag_debugmode == 1 ] && echo -e "pids of existing yad processes (\$yad_pid_before):\n$yad_pid_before\npid of yad info window (\$yad_pid):\n$yad_pid"
	kill $yad_pid
fi


# 4.) Unmounting selected devices
# -------------------------------

# unmount all mountpoints registered to a device with unmount flag set.
[ $flag_debugmode == 1 ] && echo -e "\n4.) Unmounting\n--------------\nNumber of devices to unmount: $(echo "$dlg_response" | tr '|' '\n' |grep -c 'TRUE')"
[ $flag_dryrun == 1 ] && echo -e "\033[1;33mDry run. Nothing was actually unmounted.\nOmit option -p for real operation.\033[0m"

# run through devices while checking its unmount flag
v=0
while [ $v -lt $count_items ]; do
	if [ "$(echo "$dlg_response" | cut -d '|' -f $(($v+1)))" == "TRUE" ]; then
		[ $flag_debugmode == 1 ] && echo "Unmounting device ${mnt_devices_paths[$v]}"
		count_part="$(echo "${mnt_partitions_names[$v]}" | tr '|' '\n' | wc -l)"
		# run through each partition of recent device
		w=0
		while [ $((++w)) -le $count_part ]; do
			# seperate correlated mountpoints from string
			mountpoints_paths_part="$(echo "${mountpoints_paths[$v]}" | cut -d"|" -f $w | tr '\t' '\n')"
			# run through each mountpoint of recent partition
			u=0
			while read -d"$CHR_0x0A" item; do
				# umout each mountpoint of recent device
				if [ -n "$item" ]; then
					[ $flag_debugmode == 1 ] && echo -e " -- Mountpoint:\t$item"
					if ! $flag_nosudo; then
						[ $flag_dryrun != 1 ] && umount "$item"								# here selected devices get unmounted actually (for sudoed execution)
					else
						[ $flag_dryrun != 1 ] && gksudo -u root -- umount "$item"					# here selected devices get unmounted actually (for unsudoed execution)
					fi
				fi
			done <<<"$mountpoints_paths_part"
			if ${mnt_devices_rotational[$v]}; then flag_spindown=1; fi			# set flag for spindown if one of the devices is marked as rotational
		done
	fi
	let $((v++))
done


# 5.) Test for remaining mounted partitions
# -----------------------------------------------------------------------

# check all unmounted devices against all currtently mounted devices using findmnt command to get reliable data. Refering to “man mount”: >>For more robust and customizable output use findmnt(8), especially in your scripts.<<
[ $flag_debugmode == 1 ] && echo -e "\n5.) Check for remaining unmounted partitions\n--------------------------------------------"

# run through devices while checking its unmount flag, looking it up in recent mounts; the created report lists "mountpointerrorlist" and "summarylist" are meant for display purpose only.
v=0
while [ $v -lt $count_items ]; do
	if [ "$(echo "$dlg_response" | cut -d '|' -f $(($v+1)))" == "TRUE" ]; then
		checkitem="$(sudo -u $true_user -- findmnt --list -n -o SOURCE,TARGET | grep ${mnt_devices_paths[$v]})"
		[ $flag_debugmode == 1 ] &&  echo -e "checking device ${mnt_devices_names[$v]}"
		txt_list=""
		if [ -n "$checkitem" ]; then
			device_is_unmounted[$v]="false"				#(I)
			flag_umounterror=1
			echo -e $"\033[1;31mWARNING: DEVICE ${mnt_devices_paths[$v]} WAS NOT PROPERLY UNMOUNTED!\033[0m\n\033[4;37mPlease check before unplugging.\033[0m"
			# prepare final user dialog
			while read item; do
				txt_list="$txt_list\n\t$item"
			done <<<"$(findmnt --list -n -o SOURCE,TARGET | grep ${mnt_devices_paths[$v]} | tr -s ' ' | cut -d ' ' -f 2-)"
			mountpointerrorlist="$mountpointerrorlist\n<u>$txt_dlg_device ${mnt_devices_names[$v]}</u>$txt_list\n"
		else
			device_is_unmounted[$v]="true"				#(I)
			[ $flag_debugmode == 1 ] &&  echo -e "Check OK"
			# prepare final user dialog
			summarylist="$summarylist\n<u>$txt_dlg_device ${mnt_devices_names[$v]}</u>\n\t$(echo "${mountpoints_paths[$v]}" | tr '\t' '|' | tr -s '|' | sed 's/|$//;s/|/\n\t/g')"
		fi
	fi
	let $((v++))
done

# debug-check
if [ $flag_debugmode == 1 ]; then
	echo -e "\n5.1.) Modified arrays:\n------------------------"
	v=-1
	while [ $((++v)) -lt $count_items ]; do
		echo -e "\n-- for device: $v"
		echo -e "(I) \${device_is_unmounted[$v]}:\t${device_is_unmounted[$v]}"			#(I)
	done
fi


# 6.) Spin down rotational devices and wait for them reporting to be spun down.
# spin down device if it is marked as rotating. Otherwise it is still NOT safe to unplug.
# -----------------------------------------------------------------------------

# check by flags whether spindown is needed and wanted.
if [ $flag_ignore_rotational == 1 ]; then flag_spindown=0; [ $flag_debugmode == 1 ] && echo -e "Ignoring rotational devices in need for spindown per user request"; fi
if [ "$flag_unsafe" = "false" ] && [ $flag_spindown == 1 ]; then
	# inform user first, both on console and in GUI
	echo -e $"Please wait until device(s) spun down..."
	# get yad pid of specific yad window ($! will fail on yad windows)
	yad_pid_before="$(pidof -x yad) "			# blank suffix intentionally
	sudo -u $true_user -- yad --title="$title" --fixed $centred --mouse $decorations \
		--text=$"Rotational devices\nspin down. <b>Please wait...</b>" \
		--window-icon="$icon_taskbar" \
		--no-buttons &
	sleep 1										# wait for second yad pid
	yad_pid="$(pidof -x yad)"
	if [ "$yad_pid_before" != " " ]; then		# if no items sed will fail
		while read -d' ' item
		do
			yad_pid="$(echo "$yad_pid" | sed "s/$item//g")"
		done <<<"$yad_pid_before"
	fi
	[ $flag_debugmode == 1 ] && echo -e "6.1) Spinning down rotationals\nItems in spindownlist: $(echo "$spindownlist" | wc -w)"


	# run through devices while checking rotational flag, unmount flag and unmounted flag. The created list spinningdownlist is meant for display purpose only.
	v=0
	while [ $v -lt $count_items ]; do
		if ${mnt_devices_rotational[$v]}; then
			if [ "$(echo "$dlg_response" | cut -d '|' -f $(($v+1)))" == "TRUE" ]; then
				if ${device_is_unmounted[$v]}; then
					[ $flag_debugmode == 1 ] && echo -e "Spinning down Device ${mnt_devices_names[$v]}, waiting for response."
					if ! $flag_nosudo; then
						hdparm -Y "${mnt_devices_paths[$v]}"			# (for sudoed execution) we can safely spin down rotational devices even if still mounted (can only happen in dryrun mode), they will power up again when accessed.
					else
						gksudo -u root -- hdparm -Y "${mnt_devices_paths[$v]}"	# (for non-sudoed execution) we can safely spin down rotational devices even if still mounted (can only happen in dryrun mode), they will power up again when accessed.
					fi
					response=$?
					if [ $response == 0 ];then
						[ $flag_debugmode == 1 ] && echo -e "-- Device ${mnt_devices_names[$v]} states to comply with spindown command."
					else
						[ $flag_debugmode == 1 ] && echo -e "Device ${mnt_devices_names[$v]} does not respond correctly to spindown command, while claiming to be rotational."
						txt_rotationalerror="$txt_rotationalerror${mnt_devices_names[$v]} "
					fi
				fi
			fi
		fi
		let $((v++))
	done		
	[ $flag_debugmode == 1 ] && echo -e "Spindown for all requested devices done.\npids of existing yad processes (\$yad_pid_before):\n$yad_pid_before\npid of yad info window (\$yad_pid):\n$yad_pid"
	kill $yad_pid
fi		


# 7.) Final user dialog on exit
# -----------------------------

# Preparations for dialog window
if [ "$txt_rotationalerror" != "" ]; then
	successtext=$"<big><b>Unmounted:</b></big>\n$summarylist\n\nBut despite the following devices were reported to be rotational\nthey <span foreground='red'>did not respond to spindown command</span>:\n\t$txt_rotationalerror\nPlease check whether they are spun down before unplugging.\n<b>After rotational drives are spun down it is safe to unplug.</b>"
	yad_timeout=""
	yad_button="--button="OK""
else
	successtext=$"<big><b>Unmounted:</b></big>\n$summarylist\n\n<b>It is safe to unplug the drive(s)</b>"
	yad_timeout="--timeout=5"
	yad_button="--no-buttons"
fi

# User information on exit:
if [ $flag_umounterror == 1 ];then
	# Display a message if unmount failed
	mountpointerrorlist="$(echo -e $mountpointerrorlist |tr -s '\n')"
	sudo -u $true_user -- yad --title="$title" --fixed $centred --mouse $decorations --borders=5 \
	    --text=$"<b><big>Mountpoint removal failed.</big></b>\n<span foreground='red'><u><b>One or more mountpoin(s) remain present at:</b></u></span>""\n$mountpointerrorlist\n\n"$"<b>Check each mountpoint listed before unpluging the drive(s).</b>" \
        --window-icon="$icon_taskbar" \
        --button="OK"
		# no need for checking button selection anymore
	cleanup
	exit 1
else
   # Display a message if unmount successful   
	sudo -u $true_user -- yad --title="$title" --fixed $centred $yad_timeout --mouse $decorations --borders=5 \
        --text="$successtext" \
        --window-icon="$icon_taskbar" \
        $yad_button
        # no need for checking button selection anymore
	cleanup
	exit 0
fi


