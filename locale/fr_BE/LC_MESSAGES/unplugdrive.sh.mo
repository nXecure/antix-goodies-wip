��          4      L       `   !   a   8   �   �  �   (   �  l   �                      Usage: unplugdrive.sh [options] Data is being written\nto devices. <b>Please wait...</b> Project-Id-Version: unplugdrive.sh 0.83
Report-Msgid-Bugs-To: forum.antiXlinux.com
PO-Revision-Date: 2021-07-02 09:58+0200
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: French (Belgium) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
   Utilisation : unplugdrive.sh [Options] Des données sont en train d'être\nécrites sur des périphériques.\n<b>S'il vous plaît, patientez...</b> 