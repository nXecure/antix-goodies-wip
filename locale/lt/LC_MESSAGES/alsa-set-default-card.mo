��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  7  �  /   �  1         2      O      p  #   �     �     �     �     �     �  !     ,   0                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-29 23:41-0600
PO-Revision-Date: 2019-10-12 17:17+0300
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/anticapitalista/antix-development/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 1.8.11
 (Pavyko prisijungti prie pasirinkto įrenginio) (Nepavyko prisijungti prie pasirinkto įrenginio) Šiuo metu numatytoji yra %s Nerasta jokių garso plokščių Rasta tik viena garso plokštė. Prašome pasirinkti garso plokštę Išeiti Garso plokštė nustatyta į %s Garso bandymas Bandymas nepavyko Bandymas pavyko Bandomas garsas iki 6 sekundžių Ar norėtumėte išbandyti ar veikia garsas? 