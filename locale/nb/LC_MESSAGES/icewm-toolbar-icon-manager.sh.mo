��          �            x     y     �  0   �     �     �  d   �  D   +  H   p     �  e   �     +  "   J     m     r     z  �  �     '     5  8   >     w       d   �  A   �  I   ,  
   v  f   �  .   �  0        H     N     W                                             
         	                         ADD icon ADVANCED Double click any Application to remove its icon. EXIT HELP No changes were made!\nPlease choose an application's .desktop file to add its icon the the toolbar. No changes were made!\nTIP: you can always try the Advanced buttton. Please select any option from the buttons below to manage Toolbar icons. REMOVE icon To add a new icon to the toolbar choose your applications's .desktop file then click the 'Ok' button. Toolbar Icon Manager for IceWM Toolbar Icon Manager for IceWM v.9 UNDO Warning file is empty Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 15:35+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 LEGG TIL ikon AVANSERT Dobbeltklikk et vilkårlig program for å fjerne ikonet. AVSLUTT HJELP Ingen endringer ble utført.\nVelg programmets .desktop-fil for å legge til ikonet i verktøylinja. Ingen endringer ble utført.\nHint: Forsøk knappen «Avansert». Velg en handling fra knappene under for å behandle verktøylinjeikonene. FJERN ikon For å legge til et nytt ikon til verktøylinja, velg programmets .desktop-fil og deretter OK-knappen. TIM – Behandle verktøylinjeikoner for IceWM Behandle verktøylinjeikoner for IceWM (TIM) v.9 ANGRE Advarsel filen er tom 