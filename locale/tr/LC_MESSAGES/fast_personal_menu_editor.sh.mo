��          |      �             !     2  �   D  B   �  $   	  3   .     b     s  F   �     �     �  �  �     x     �  �   �  X   G  .   �  ;   �            ;   2     n     �                                 	             
             ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 15:46+0300
Last-Translator: mahmut özcan <mahmutozcan@protonmail.com>, 2020
Language-Team: Turkish (https://www.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Seçilen uygulamayı EKLE Uygulama .desktop dosyası Kişisel menüye eklemek istediğiniz .desktop dosyasını seçin (yada aşağıdaki alana sürükleyip bırakın) \n VEYA başka bir seçenek belirleyin FTM, kişisel menü dosyasında daima 1 satır yer tutacak şekilde programlanmıştır! IceWM için Hızlı Kişisel Menü Yöneticisi Hiç değişiklik yapılmadı! Lütfen bir uygulama seçin. Girişleri DÜZENLE Son girişi KALDIR Bu kişisel menünüzden son girişi silecek! Emin misiniz? Son değişikliği GERİ AL Uyarı 